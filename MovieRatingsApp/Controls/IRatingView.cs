﻿using MvvmHelpers.Commands;

namespace MovieRatingsApp.Controls;

public interface IRatingView : IView
{
    public int Value { get; set; }

    public Color Color { get; set; }

    public int MaxValue { get; set; }

    public AsyncCommand<int> RateCommand { get; set; }
}
