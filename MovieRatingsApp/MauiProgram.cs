﻿using Microsoft.Extensions.Logging;
using MovieRatingsApp.Handlers;

namespace MovieRatingsApp
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                    fonts.AddFont("materialdesignicons-webfont.ttf", "MaterialDesignIcons");
                })
#if ANDROID
                .ConfigureMauiHandlers(handlers =>
                 {
                     handlers.AddHandler(
                         typeof(RatingView),
                         typeof(RatingViewHandler));
                 })
#endif
                ;


#if DEBUG
            builder.Logging.AddDebug();
#endif

            // dependencies section
            builder.Services.AddSingleton<IAlertService, AlertService>();
            builder.Services.AddSingleton<IMovieService, MovieService>();
            builder.Services.AddSingleton<ILibraryService, LibraryService>();

            builder.Services.AddSingleton<MovieListPage>();

            builder.Services.AddSingleton<MovieListViewModel>();

            return builder.Build();
        }
    }
}
