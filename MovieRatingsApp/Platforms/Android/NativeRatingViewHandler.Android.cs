﻿using Microsoft.Maui.Handlers;
using IRatingView = MovieRatingsApp.Controls.IRatingView;
using Android.Widget;

namespace MovieRatingsApp.Handlers;

public partial class RatingViewHandler : ViewHandler<IRatingView, NativeRatingViewAndroid>
{
    NativeRatingViewAndroid? _drawView;
    RatingBarChangeListener ChangeListener { get; } = new RatingBarChangeListener();

    protected override NativeRatingViewAndroid CreatePlatformView()
    {
        if (_drawView == null)
        {
            _drawView = new NativeRatingViewAndroid(Context);
        }

        return _drawView;
    }

    public static void MapMaxValue(IRatingViewHandler handler, IRatingView ratingView)
    {
        handler.PlatformView.RatingBar.NumStars = ratingView.MaxValue;
    }
    public static void MapValue(IRatingViewHandler handler, IRatingView ratingView)
    {
        handler.PlatformView.RatingBar.Rating = (float)ratingView.Value;
    }
    public static void MapColor(IRatingViewHandler handler, IRatingView ratingView)
    {
        handler.PlatformView?.UpdateRatingBarColor(ratingView);
    }
    protected override void ConnectHandler(NativeRatingViewAndroid platformView)
    {
        base.ConnectHandler(platformView);
        ChangeListener.Handler = this;
        platformView.RatingBar.OnRatingBarChangeListener = ChangeListener;
    }

    protected override void DisconnectHandler(NativeRatingViewAndroid platformView)
    {
        base.DisconnectHandler(platformView);
        ChangeListener.Handler = null;
        platformView.RatingBar.OnRatingBarChangeListener = null;
        PlatformView?.Dispose();
    }
    void OnProgressChanged(RatingBar ratingBar, float rating, bool fromUser)
    {
        if (VirtualView == null || !fromUser)
            return;

        VirtualView.Value = (int)rating;
        VirtualView.RateCommand.ExecuteAsync(VirtualView.Value);
    }

    internal class RatingBarChangeListener : Java.Lang.Object, RatingBar.IOnRatingBarChangeListener
    {
        public RatingViewHandler? Handler { get; set; }

        public RatingBarChangeListener()
        {
        }
        public void OnRatingChanged(RatingBar? ratingBar, float rating, bool fromUser)
        {
            if (Handler == null || ratingBar == null)
                return;

            Handler.OnProgressChanged(ratingBar, rating, fromUser);
        }
    }
}
