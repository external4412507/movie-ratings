﻿using MvvmHelpers.Commands;

namespace MovieRatingsApp.ViewModels;

public partial class MovieDetailViewModel : BaseViewModel
{
    [ObservableProperty]
    private int _id;

    [ObservableProperty]
    private string _name;

    [ObservableProperty]
    private string _description;

    [ObservableProperty]
    private int _yearReleased;

    [ObservableProperty]
    private string _image;

    [ObservableProperty]
    private int _myRating;

    [ObservableProperty]
    private string _movieType;

    [ObservableProperty]
    private string _genre;

    [ObservableProperty]
    private string _imdbId;

    private IMovieService _movieService;
    public AsyncCommand<int> RateCommand { get; }
    public MovieDetailViewModel(Movie movie, IMovieService movieService)
    {
        Id = movie.Id;
        Name = movie.Name;
        Description = movie.Description;
        YearReleased = movie.YearReleased;
        Image = movie.Image;
        MyRating = movie.MyRating;
        MovieType = movie.MovieType;
        Genre = movie.Genre;
        ImdbId = movie.ImdbId;

        _movieService = movieService;

        RateCommand = new AsyncCommand<int>(RateMovieAsync);
    }
    private async Task RateMovieAsync(int currentRating)
    {
        await _movieService.RateMovieAsync(Id, currentRating);
    }
    public override bool Equals(object obj) => this.Equals(obj as MovieDetailViewModel);

    public bool Equals(MovieDetailViewModel p)
    {
        if (p is null)
        {
            return false;
        }

        if (Object.ReferenceEquals(this, p))
        {
            return true;
        }

        if (this.GetType() != p.GetType())
        {
            return false;
        }

        return (ImdbId == p.ImdbId) && (Name == p.Name);
    }

    public override int GetHashCode() => (ImdbId, Name).GetHashCode();
}
