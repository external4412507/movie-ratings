using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace MovieRatingsApp.Views;
public partial class MovieListPage : ContentPage
{
    private MovieListViewModel _movieListViewModel;
    public MovieListPage(MovieListViewModel movieListViewModel)
    {
        InitializeComponent();
        _movieListViewModel = movieListViewModel;
        BindingContext = _movieListViewModel;

    }
    protected override async void OnAppearing()
    {
        base.OnAppearing();
        await _movieListViewModel.OnAppearing();
    }

    private void Searchbar_TextChanged(object sender, TextChangedEventArgs e)
    {
        if (string.IsNullOrEmpty(((SearchBar)sender).Text))
        {
            _movieListViewModel.MovieImdbResults.Clear();
        }   
    }
}