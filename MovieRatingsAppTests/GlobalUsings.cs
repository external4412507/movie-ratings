global using Xunit;
global using NSubstitute;
global using MovieRatingsApp.Models;
global using MovieRatingsApp.Services;
global using MovieRatingsApp.ViewModels;
global using MovieRatingsApp.Controls;
global using MovieRatingsApp.Helpers;
global using MvvmHelpers.Commands;