﻿namespace MovieRatingsApp.Services;

public class AlertService : IAlertService
{
    public Task<bool> ShowConfirmationAsync(string title, string message, string accept = "Yes", string cancel = "No")
    {
        return Application.Current.MainPage.DisplayAlert(title, message, accept, cancel);
    }
}
