﻿namespace MovieRatingsApp.Services;

public class MovieService : IMovieService
{
    private SQLiteAsyncConnection _dbConnection;
    public async Task Init()
    {
        if (_dbConnection != null)
            return;

        var databasePath = Path.Combine(FileSystem.AppDataDirectory, "MyData.db");

        _dbConnection = new SQLiteAsyncConnection(databasePath);

        await _dbConnection.CreateTableAsync<Movie>();

        bool isFirstBoot = Preferences.Default.Get("isFirstBoot", true);
        if (isFirstBoot)
        {
            var defaultMovieList = GetDefaultMovieList();
            await _dbConnection.InsertAllAsync(defaultMovieList);
            Preferences.Default.Set("isFirstBoot", false);
        }
    }
    public async Task UpsertMovieAsync(Movie item)
    {
        await Init();
        if (item.Id == 0)
        {
            await _dbConnection.InsertAsync(item);
            return;
        }

        await _dbConnection.UpdateAsync(item);
    }
    public async Task RemoveMovieAsync(int id)
    {
        await Init();
        await _dbConnection.DeleteAsync<Movie>(id);
    }
    public async Task<IEnumerable<Movie>> GetMoviesAsync()
    {
        await Init();
        var movieList = await _dbConnection.Table<Movie>().ToListAsync();
        return movieList.OrderByDescending(m => m.DateAdded);
    }
    public async Task<Movie?> GetMovieAsync(int id)
    {
        await Init();
        try
        {
            var movie = await _dbConnection.GetAsync<Movie>(id);
            return movie;
        }
        catch (Exception)
        {
            return null;
        }
    }
    public async Task RateMovieAsync(int id, int rating)
    {
        var movie = await GetMovieAsync(id);
        if (movie is not null)
        {
            movie.MyRating = rating;
            await _dbConnection.UpdateAsync(movie);
        }

    }

    private List<Movie> GetDefaultMovieList()
    {
        var movies = new List<Movie>() {
                new Movie() {
                Name            = "Jack Ryan",
                Description     = "Tom Clancy's Jack Ryan (also known simply as Jack Ryan) is an American political action thriller television series based on characters from the fictional Ryanverse created by Tom Clancy. ",
                YearReleased    = 2017,
                Image           = "https://upload.wikimedia.org/wikipedia/en/1/12/Jack_Ryan.png",
                MyRating        = 0,
                Genre           = "Action",
                MovieType       = "TV Series",
                ImdbId          = "tjackr111"},

                new Movie() {
                    Name            = "Griselda",
                    Description     = "Griselda chronicles the real life of savvy and ambitious Colombian business woman, Griselda Blanco, who created one of the most profitable cartels in history.",
                    YearReleased    = 2024,
                    Image           = "https://upload.wikimedia.org/wikipedia/en/8/89/Griselda_%28miniseries%29.jpg",
                    MyRating        = 0,
                    Genre           = "Drama",
                    MovieType       = "Mini Series",
                    ImdbId          = "tgrsld222"} };

        return movies;
    }
}
