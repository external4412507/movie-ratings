﻿#if IOS || MACCATALYST
#elif ANDROID
#elif WINDOWS
#elif (NETSTANDARD || !PLATFORM) || (NET8_0 && !IOS && !ANDROID)

using Microsoft.Maui.Handlers;

using IRatingView = MovieRatingsApp.Controls.IRatingView;

namespace MovieRatingsApp.Handlers;
public partial class RatingViewHandler : ViewHandler<IRatingView, object>
{
    protected override object CreatePlatformView() => throw new NotImplementedException();

    public static void MapMaxValue(IRatingViewHandler handler, IRatingView ratingView)
    {

    }
    public static void MapValue(IRatingViewHandler handler, IRatingView ratingView)
    {

    }
    public static void MapColor(IRatingViewHandler handler, IRatingView ratingView)
    {

    }
}
#endif