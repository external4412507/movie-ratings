﻿namespace MovieRatingsApp.Services;

public interface IMovieService
{
    Task<Movie?> GetMovieAsync(int id);
    Task<IEnumerable<Movie>> GetMoviesAsync();
    Task RemoveMovieAsync(int id);
    Task UpsertMovieAsync(Movie item);
    Task RateMovieAsync(int id, int rating);
}
