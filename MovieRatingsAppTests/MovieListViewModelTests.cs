﻿namespace MovieRatingsAppTests;

public class MovieListViewModelTests
{
    [Fact]
    public async Task TestOnAppearing()
    {
        IMovieService movieService = GetMockMovieService();
        ILibraryService libraryService = GetMockLibraryService();
        IAlertService alertService = GetMockAlertService();
        var viewModel = new MovieListViewModel(movieService, libraryService, alertService);

        await viewModel.OnAppearing();

        Assert.Equal(2, viewModel.Movies.Count);
    }

    [Fact]
    public async Task TestSearchMovieValid()
    {
        IMovieService movieService = GetMockMovieService();
        ILibraryService libraryService = GetMockLibraryService();
        IAlertService alertService = GetMockAlertService();
        var viewModel = new MovieListViewModel(movieService, libraryService, alertService);
        await viewModel.OnAppearing();

        await viewModel.PerformSearchAsync("Jack");

        Assert.Single(viewModel.MovieImdbResults);
    }

    [Fact]
    public async Task TestSearchMovieInValid()
    {
        IMovieService movieService = GetMockMovieService();
        ILibraryService libraryService = GetMockLibraryService();
        IAlertService alertService = GetMockAlertService();
        var viewModel = new MovieListViewModel(movieService, libraryService, alertService);
        await viewModel.OnAppearing();

        await viewModel.PerformSearchAsync("");

        Assert.Empty(viewModel.MovieImdbResults);
    }

    [Fact]
    public async Task TestDeleteMovie()
    {
        IMovieService movieService = GetMockMovieService();
        ILibraryService libraryService = GetMockLibraryService();
        IAlertService alertService = GetMockAlertService();
        
        var viewModel = new MovieListViewModel(movieService, libraryService, alertService);
        await viewModel.OnAppearing();

        alertService.ShowConfirmationAsync("Delete movie", $"Are you sure you want to delete {viewModel.Movies.First().Name}").Returns(false);
        alertService.ShowConfirmationAsync("Delete movie", $"Are you sure you want to delete {viewModel.Movies[1].Name}").Returns(true);
        
        //delete without confirmation does not delete
        await viewModel.DeleteMovie(viewModel.Movies.First());
        Assert.Equal(2, viewModel.Movies.Count);

        //delete with confirmation deletes the movie
        await viewModel.DeleteMovie(viewModel.Movies[1]);
        Assert.Single(viewModel.Movies);

        //we can search for the deleted movie
        await viewModel.PerformSearchAsync("Gri");
        Assert.Equal(2, viewModel.MovieImdbResults.Count);
    }

    [Fact]
    public async Task TestAddMovie()
    {
        IMovieService movieService = GetMockMovieService();
        ILibraryService libraryService = GetMockLibraryService();
        IAlertService alertService = GetMockAlertService();
        var viewModel = new MovieListViewModel(movieService, libraryService, alertService);
        await viewModel.OnAppearing();

        await viewModel.PerformSearchAsync("Jack");
        await viewModel.SelectImdbMovieAsync(viewModel.MovieImdbResults.First());

        Assert.Equal(3, viewModel.Movies.Count);
    }

    private static IAlertService GetMockAlertService()
    {
        var alertService = Substitute.For<IAlertService>();
        alertService.ShowConfirmationAsync("Delete", "Delete").Returns(true);
        return alertService;
    }

    private static ILibraryService GetMockLibraryService()
    {
        var libraryService = Substitute.For<ILibraryService>();
        libraryService.SearchForMoviesByNameAsync("Jack").Returns(new MovieImdbResultsList()
        {
            results = new List<MovieImdbResult> 
            { 
                new MovieImdbResult() { imdb_id = "t111", title = "Jack Ryan" },
                new MovieImdbResult() { imdb_id = "t333", title = "Jack Reacher"},
            }
        }
        );
        libraryService.SearchForMoviesByNameAsync("Gri").Returns(new MovieImdbResultsList()
        {
            results = new List<MovieImdbResult>
            {
                new MovieImdbResult() { imdb_id = "t222", title = "Griselda"},
                new MovieImdbResult() { imdb_id = "t444", title = "Grizzly"}
            }
        }
        );
        libraryService.SearchForMoviesByNameAsync("").Returns(new MovieImdbResultsList()
        {
            results = new List<MovieImdbResult>
            {           
            }
        }
        );
        libraryService.SearchForMoviesByIdAsync("t333").Returns(new MovieImdbContentResult()
        {
            results = new MovieImdbContent
            {
                title = "Jack Reacher",
                description = "Jack reacher description",
                imdb_id = "t333"
            }
        });
        return libraryService;
    }

    private static IMovieService GetMockMovieService()
    {
        var movieService = Substitute.For<IMovieService>();
        movieService.GetMoviesAsync().Returns(new List<MovieRatingsApp.Models.Movie>()
        {
                    new Movie() {Id             = 0, 
                                 Name           = "Jack Ryan",
                                 Description    = "Tom Clancy's Jack Ryan (also known simply as Jack Ryan) is an American political action thriller television series based on characters from the fictional Ryanverse created by Tom Clancy. ",
                                 YearReleased   = 2017,
                                 Image          = "https://upload.wikimedia.org/wikipedia/en/1/12/Jack_Ryan.png",
                                 MyRating       = 0,
                                 Genre          = "Action",
                                 MovieType      = "TV Series",
                                 ImdbId         = "t111"},

                    new Movie() {Id             = 0, 
                                 Name           = "Griselda",
                                 Description    = "Griselda chronicles the real life of savvy and ambitious Colombian business woman, Griselda Blanco, who created one of the most profitable cartels in history.",
                                 YearReleased   = 2024,
                                 Image          = "https://upload.wikimedia.org/wikipedia/en/8/89/Griselda_%28miniseries%29.jpg",
                                 MyRating       = 0,
                                 Genre          = "Drama",
                                 MovieType      = "Mini Series",
                                 ImdbId         = "t222"},

        });
        return movieService;
    }
}
