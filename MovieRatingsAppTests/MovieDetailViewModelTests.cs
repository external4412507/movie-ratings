﻿namespace MovieRatingsAppTests;

public class MovieDetailViewModelTests
{
    [Fact]
    public void TestContructor()
    {
        string name         = "Test movie name";
        string description  = "Test movie description";
        int yearReleased    = 2000;
        string image        = "Test image url";
        string movieType    = "movie";
        string genre        = "comedy";
        int rating          = 3;
        string imdbId       = "ttt123";
        var movie = new Movie { Name            = name, 
                                Description     = description, 
                                YearReleased    = yearReleased, 
                                Image           = image, 
                                MovieType       = movieType, 
                                Genre           = genre, 
                                MyRating        = rating,
                                ImdbId          = imdbId};

        var movieService = Substitute.For<IMovieService>();
        var viewModel = new MovieDetailViewModel(movie, movieService);


        Assert.Equal(name, viewModel.Name);
        Assert.Equal(description, viewModel.Description);
        Assert.Equal(yearReleased, viewModel.YearReleased);
        Assert.Equal(image, viewModel.Image);
        Assert.Equal(movieType, viewModel.MovieType);
        Assert.Equal(genre, viewModel.Genre);
        Assert.Equal(rating, viewModel.MyRating);
        Assert.Equal(imdbId, viewModel.ImdbId);
    }
}
