﻿namespace MovieRatingsApp.ViewModels;

public partial class MovieListViewModel : BaseViewModel
{
    private IMovieService _movieService;
    private ILibraryService _libraryService;
    private IAlertService _alertService;
    private HashSet<string> _moviesAlreadyAdded;

    public ObservableRangeCollection<MovieDetailViewModel> Movies { get; } = new();
    public ObservableRangeCollection<MovieImdbResult> MovieImdbResults { get; } = new();
    public MovieListViewModel(IMovieService movieService, ILibraryService libraryService, IAlertService alertService)
    {
        Title = "Movies";
        _movieService = movieService;
        _libraryService = libraryService;
        _alertService = alertService;
        _moviesAlreadyAdded = new HashSet<string>();
    }
    public async Task OnAppearing()
    {
        await LoadFavouriteMovieList();
    }

    private async Task LoadFavouriteMovieList()
    {
        IsBusy = true;
        if (Movies.Count != 0)
            Movies.Clear();

        List<MovieDetailViewModel> movieDetailList = new List<MovieDetailViewModel>();

        var movies = await _movieService.GetMoviesAsync();

//On windows, collection view doen't behave properly when it is bound to an 
//observable collection loaded with AddRange, so I use Add instead
#if WINDOWS
        foreach (var movie in movies)
        {
            Movies.Add(new MovieDetailViewModel(movie, this._movieService));
            _moviesAlreadyAdded.Add(movie.ImdbId);
         }
#else
        foreach (var movie in movies)
        {
            movieDetailList.Add(new MovieDetailViewModel(movie, this._movieService));
            _moviesAlreadyAdded.Add(movie.ImdbId);
        }

        Movies.AddRange(movieDetailList);
#endif
        IsBusy = false;
    }
    private Movie ConvertImdbResultToMovie(MovieImdbContentResult movieImdbContentResult)
    {
        MovieImdbContent movieResult = movieImdbContentResult.results;
        var movie = new Movie { ImdbId          = movieResult.imdb_id,
                                Name            = movieResult.title, 
                                Description     = movieResult.description, 
                                YearReleased    = movieResult.year, 
                                Image           = movieResult.image_url, 
                                MovieType       = movieResult.type, 
                                Genre           = movieResult.gen != null && movieResult.gen.Count > 0 ? movieResult.gen[0].genre : string.Empty };
        return movie;
    }

    [RelayCommand]
    public async Task PerformSearchAsync(string query)
    {
        if (IsBusy)
            return;

        if (MovieImdbResults.Count != 0)
            MovieImdbResults.Clear();

        var movieResults = await _libraryService.SearchForMoviesByNameAsync(query);
        if (movieResults == null || movieResults.results.Count == 0)
            return;

        MovieImdbResults.AddRange(movieResults.results.Where(m => !_moviesAlreadyAdded.Contains(m.imdb_id)).ToList());
    }

    [RelayCommand]
    public async Task SelectImdbMovieAsync(object args)
    {
        IsBusy = true;
        var movieImdbResult = args as MovieImdbResult;
        if (movieImdbResult == null)
            return;

        var movieResults = await _libraryService.SearchForMoviesByIdAsync(movieImdbResult.imdb_id);
        if (movieResults == null)
            return;

        await AddMovieAsync(movieResults);

        MovieImdbResults.Clear();
        IsBusy = false;
    }

    [RelayCommand]
    public async Task DeleteMovie(object args)
    {
        var movie = args as MovieDetailViewModel;
        if (movie == null)
            return;

        if (await _alertService.ShowConfirmationAsync("Delete movie", $"Are you sure you want to delete {movie.Name}"))
        {
            await RemoveMovieAsync(movie);
        }

    }
    private async Task RemoveMovieAsync(MovieDetailViewModel movie)
    {
        await _movieService.RemoveMovieAsync(movie.Id);
        Movies.Remove(movie);
        _moviesAlreadyAdded.Remove(movie.ImdbId);
    }
    private async Task AddMovieAsync(MovieImdbContentResult movieResults)
    {
        var movie = ConvertImdbResultToMovie(movieResults);
        await _movieService.UpsertMovieAsync(movie);
        Movies.Insert(0, new MovieDetailViewModel(movie, this._movieService));
        _moviesAlreadyAdded.Add(movie.ImdbId);
    }
}
