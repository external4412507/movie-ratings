﻿namespace MovieRatingsApp.Services;

public interface IAlertService
{
    Task<bool> ShowConfirmationAsync(string title, string message, string accept = "Yes", string cancel = "No");
}
