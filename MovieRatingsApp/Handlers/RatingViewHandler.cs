﻿#if IOS || MACCATALYST
using PlatformView = MovieRatingsApp.Controls.NativeRatingViewiOS;
#elif ANDROID
using PlatformView = MovieRatingsApp.Controls.NativeRatingViewAndroid;
#elif WINDOWS
using PlatformView =  MovieRatingsApp.Controls.NativeRatingViewWindows;
#elif (NETSTANDARD || !PLATFORM) || (NET8_0 && !IOS && !ANDROID)
using PlatformView = System.Object;
#endif

using IRatingView = MovieRatingsApp.Controls.IRatingView;

using Microsoft.Maui.Handlers;
namespace MovieRatingsApp.Handlers;

public partial class RatingViewHandler : IRatingViewHandler
{
    public static IPropertyMapper<IRatingView, IRatingViewHandler> Mapper =
    new PropertyMapper<IRatingView, IRatingViewHandler>(ViewHandler.ViewMapper)
    {
        [nameof(IRatingView.Value)] = MapValue,
        [nameof(IRatingView.Color)] = MapColor,
        [nameof(IRatingView.MaxValue)] = MapMaxValue,
    };

    public static CommandMapper<IRatingView, IRatingViewHandler> CommandMapper =  new (ViewCommandMapper)
    {
    
    };
    public RatingViewHandler() : base(Mapper, CommandMapper)
    {
    }
    IRatingView IRatingViewHandler.VirtualView => VirtualView;

    PlatformView IRatingViewHandler.PlatformView => PlatformView;
}
