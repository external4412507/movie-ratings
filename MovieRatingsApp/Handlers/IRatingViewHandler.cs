﻿#if IOS || MACCATALYST
using PlatformView = MovieRatingsApp.Controls.NativeRatingViewiOS;
#elif ANDROID
using PlatformView = MovieRatingsApp.Controls.NativeRatingViewAndroid;
#elif WINDOWS
using PlatformView = MovieRatingsApp.Controls.NativeRatingViewWindows;
#elif (NETSTANDARD || !PLATFORM) || (NET8_0 && !IOS && !ANDROID)
using PlatformView = System.Object;
#endif

using IRatingView = MovieRatingsApp.Controls.IRatingView;

namespace MovieRatingsApp.Handlers;

public interface IRatingViewHandler : IViewHandler
{
    new IRatingView VirtualView { get; }
    new PlatformView PlatformView { get; }
}
