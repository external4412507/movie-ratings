﻿namespace MovieRatingsApp.Models;

public class Movie
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int YearReleased { get; set; }
    public string Image { get; set; }
    public int MyRating { get; set; }
    public string MovieType { get; set; }
    public string Genre { get; set; }
    public string ImdbId { get; set; }
    public DateTime DateAdded { get; set; } = DateTime.Now;
}

