﻿namespace MovieRatingsApp.Services;

public interface ILibraryService
{
    public Task<MovieImdbResultsList?> SearchForMoviesByNameAsync(string name);

    public Task<MovieImdbContentResult?> SearchForMoviesByIdAsync(string imdb_id);
}
