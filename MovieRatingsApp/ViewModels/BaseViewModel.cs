﻿namespace MovieRatingsApp.ViewModels;

public partial class BaseViewModel : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    [ObservableProperty]
    bool isBusy;

    [ObservableProperty]
    string title;
}
