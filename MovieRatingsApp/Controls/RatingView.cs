﻿using MvvmHelpers.Commands;

namespace MovieRatingsApp.Controls;

public class RatingView : HorizontalStackLayout, IRatingView
{
    // public properties
    public int Value
    {
        get => (int)GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public int MaxValue
    {
        get => (int)GetValue(MaxValueProperty);
        set => SetValue(MaxValueProperty, value);
    }

    public double RateIconSize
    {
        get => (double)GetValue(RateIconSizeProperty);
        set => SetValue(RateIconSizeProperty, value);
    }

    public Color Color
    {
        get => (Color)GetValue(ColorProperty);
        set => SetValue(ColorProperty, value);
    }

    public AsyncCommand<int> RateCommand
    {
        get => (AsyncCommand<int>)GetValue(RateCommandProperty);
        set => SetValue(RateCommandProperty, value);
    }

    // bindable properties
    public static readonly BindableProperty ValueProperty =
    BindableProperty.Create(
        nameof(Value),
        typeof(int),
        typeof(RatingView),
        defaultValue: 0,
        propertyChanged: OnRefreshControl,
        coerceValue: CoerceValue,
        defaultBindingMode: BindingMode.TwoWay
        );

    static object CoerceValue(BindableObject bindable, object value)
    {
        RatingView? ratingControl = bindable as RatingView;
        int input = (int)value;
        var maxVal = ratingControl?.MaxValue ?? 5;

        return Math.Max(0, Math.Min(input, maxVal));
    }

    public static readonly BindableProperty MaxValueProperty =
      BindableProperty.Create(
          nameof(MaxValue),
          typeof(int),
          typeof(RatingView),
          defaultValue: 5,
          coerceValue: CoerceMaxValue,
          propertyChanged: OnRefreshControl);

    static object CoerceMaxValue(BindableObject bindable, object value)
    {
        if (bindable is not RatingView)
            return 0;

        int input = (int)value;
        if (input < 1 || input > 10)
        {
            return 5;
        }

        return input;
    }

    public static readonly BindableProperty RateIconSizeProperty =
      BindableProperty.Create(
          nameof(RateIconSize),
          typeof(double),
          typeof(RatingView),
          defaultValue: 30d,
          propertyChanged: OnRefreshControl);

    public static readonly BindableProperty ColorProperty =
        BindableProperty.Create(
            nameof(Color),
            typeof(Color),
            typeof(RatingView),
            defaultValue: Colors.Orange,
            propertyChanged: OnRefreshControl);

    public static readonly BindableProperty RateCommandProperty =
    BindableProperty.Create(
        nameof(RateCommand),
        typeof(AsyncCommand<int>),
        typeof(RatingView),
        defaultBindingMode: BindingMode.TwoWay,
        propertyChanged: OnCommandPropertyChanged);

    // property changed handlers
    static void OnCommandPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
        if (bindable is RatingView ratingControl)
        {
            ratingControl.RateCommand = (AsyncCommand<int>)newValue;
        }
    }
    private static void OnRefreshControl(BindableObject bindable, object oldValue, object newValue)
    {
        if (bindable is RatingView ratingControl)
        {
            ratingControl.Value = Math.Max(0, Math.Min(ratingControl.Value, ratingControl.MaxValue));
            ratingControl.DisplayRatingControl();
        }
    }
    public RatingView()
    {
        Spacing = -2;
        DisplayRatingControl();
    }

    private void DisplayRatingControl()
    {
        Children.Clear();

        for (int i = 0; i < Value; i++)
        {
            Add(CreateStarLabel(RateIconState.Full, i + 1));
        }

        for (int i = Value; i < MaxValue; i++)
        {
            Add(CreateStarLabel(RateIconState.Empty, i + 1));
        }
    }

    private Label CreateStarLabel(RateIconState state, int labelPosition)
    {
        var label = new Label
        {
            FontFamily = "MaterialDesignIcons",
            TextColor = Color,
            FontSize = RateIconSize,
            Text = state switch
            {
                RateIconState.Empty => MaterialDesignIconsFont.StarOutline,
                RateIconState.Full => MaterialDesignIconsFont.Star,
                _ => MaterialDesignIconsFont.StarOutline,
            },
            
        };

        TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
        tapGestureRecognizer.CommandParameter = labelPosition;
        tapGestureRecognizer.Command = new MvvmHelpers.Commands.Command<int>(DrawLabels);
        label.GestureRecognizers.Add(tapGestureRecognizer);

        return label;
    }

    private void DrawLabels(int selectedLabelPosition)
    {
        // enable the user to unrate
        int selectedRating = selectedLabelPosition;
        if (selectedLabelPosition == 1 && Value == 1)
        {
            selectedRating = 0;
        }
        Value = selectedRating;
        RateCommand?.ExecuteAsync(selectedRating);
    }
}
