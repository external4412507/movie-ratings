﻿using System.Text.Json;

namespace MovieRatingsApp.Services;

public class LibraryService: ILibraryService
{
    private HttpClient _httpClient;
    public LibraryService()
    {
        _httpClient = new HttpClient();
    }

    public async Task<MovieImdbResultsList?> SearchForMoviesByNameAsync(string name)
    {
        MovieImdbResultsList? imdbResults = null;

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"https://moviesminidatabase.p.rapidapi.com/movie/imdb_id/byTitle/{name}/"),
                Headers =
                {
                        { "X-RapidAPI-Key", "edbb9589femshae66a2718e5b449p1d4b88jsn72713d546a62" },
                        { "X-RapidAPI-Host", "moviesminidatabase.p.rapidapi.com" },
                },
            };


            using (var response = await _httpClient.SendAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    imdbResults = JsonSerializer.Deserialize<MovieImdbResultsList>(body);
                    if (imdbResults != null)
                    {
                        imdbResults.results = imdbResults.results.Take(10).ToList();
                    }
                }
            }
        }
        catch (Exception) { }

        return imdbResults;
    }

    public async Task<MovieImdbContentResult?> SearchForMoviesByIdAsync(string imdb_id)
    {
        MovieImdbContentResult? imdbResults = null;

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"https://moviesminidatabase.p.rapidapi.com/movie/id/{imdb_id}/"),
                Headers =
                {
                    { "X-RapidAPI-Key", "edbb9589femshae66a2718e5b449p1d4b88jsn72713d546a62" },
                    { "X-RapidAPI-Host", "moviesminidatabase.p.rapidapi.com" },
                },
            };


            using (var response = await _httpClient.SendAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    imdbResults = JsonSerializer.Deserialize<MovieImdbContentResult>(body);
                }
            }
        }
        catch (Exception) { }


        return imdbResults;
    }
}
