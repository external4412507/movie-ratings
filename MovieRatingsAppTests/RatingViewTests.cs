namespace MovieRatingsAppTests;

public class RatingViewTests
{
    [Fact]
    public void TestValidInitialization()
    {
        RatingView ratingView = new RatingView();

        Assert.Equal(0, ratingView.Value);
        Assert.Equal(5, ratingView.MaxValue);
        Assert.Equal(30, ratingView.RateIconSize);
        Assert.Equal(Colors.Orange, ratingView.Color);
    }

    [Fact]
    public void TestInvalidValueMoreThanMax()
    {
        RatingView ratingView = new RatingView();

        ratingView.MaxValue = 5;
        ratingView.Value = 10;

        Assert.Equal(ratingView.MaxValue, ratingView.Value);
    }

    [Fact]
    public void TestInvalidValueLessThanMin()
    {
        RatingView ratingView = new RatingView();

        ratingView.Value = -10;

        Assert.Equal(0, ratingView.Value);
    }

    [Fact]
    public void TestInvalidMaxValueLessThanMin()
    {
        RatingView ratingView = new RatingView();

        ratingView.MaxValue = -10;

        Assert.Equal(5, ratingView.MaxValue);
    }

    [Fact]
    public void TestInvalidMaxValueMoreThanMax()
    {
        RatingView ratingView = new RatingView();

        ratingView.MaxValue = 20;

        Assert.Equal(5, ratingView.MaxValue);
    }

    [Fact]
    public void TestDisplayRating()
    {
        RatingView ratingView = new RatingView();

        const int rating = 3;
        ratingView.Value = rating;

        Assert.Equal(ratingView.MaxValue, ratingView.Children.Count);
        Assert.Equal(rating, ratingView.Children.Count(c => (c as Label)?.Text == MaterialDesignIconsFont.Star));
        Assert.Equal(ratingView.MaxValue - rating, ratingView.Children.Count(c => (c as Label)?.Text == MaterialDesignIconsFont.StarOutline));
    }


    [Fact]
    public void TestRatingCommandExecute_ValueUpdated()
    {
        RatingView ratingView = new RatingView();
        var commandExecuted = false;
        const int rating = 2;
        ratingView.RateCommand = new AsyncCommand<int>( (rating) => { commandExecuted = true; return Task.CompletedTask; });
        ((ratingView.Children[rating - 1] as Label)?.GestureRecognizers[0] as TapGestureRecognizer)?.Command?.Execute(rating);

        Assert.True(commandExecuted);
        //check that Value was updated to the clicked star value
        Assert.Equal(rating, ratingView.Children.Count(c => (c as Label)?.Text == MaterialDesignIconsFont.Star));
        Assert.Equal(ratingView.MaxValue - rating, ratingView.Children.Count(c => (c as Label)?.Text == MaterialDesignIconsFont.StarOutline));
    }
}