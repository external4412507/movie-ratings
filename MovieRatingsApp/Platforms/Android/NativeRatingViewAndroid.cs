﻿using Android.Content;
using Android.Util;
using Android.Widget;
using Android.Content.Res;
using Color = Microsoft.Maui.Graphics.Color;
using Microsoft.Maui.Platform;

namespace MovieRatingsApp.Controls;
public class NativeRatingViewAndroid : LinearLayout
{
    private RatingBar _ratingBar;
    
    public RatingBar RatingBar
    {
        get => _ratingBar;
    }
    public NativeRatingViewAndroid(Context context) : base(context)
    {
        _ratingBar = new RatingBar(context, null, Android.Resource.Attribute.RatingBarStyleSmall);
        _ratingBar.IsIndicator = false;
        _ratingBar.StepSize = 1;

        Initialize();
    }
    public NativeRatingViewAndroid(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
    {
        Initialize();
    }

    public NativeRatingViewAndroid(Context context, IAttributeSet attrs) : base(context, attrs)
    {
        Initialize();
    }

    private void Initialize()
    {
        AddView(_ratingBar, new LayoutParams(Android.Views.ViewGroup.LayoutParams.WrapContent, Android.Views.ViewGroup.LayoutParams.WrapContent));
    }

    public void UpdateRatingBarColor(IRatingView ratingView)
    {
        Color color = ratingView.Color;

        if (color == null)
        {
            (_ratingBar.Indeterminate ? _ratingBar.IndeterminateDrawable :
                _ratingBar.ProgressDrawable)?.ClearColorFilter();
        }
        else
        {
            var tintList = ColorStateList.ValueOf(color.ToPlatform());

            if (_ratingBar.Indeterminate)
                _ratingBar.IndeterminateTintList = tintList;
            else
                _ratingBar.ProgressTintList = tintList;
        }
    }
}
