﻿namespace MovieRatingsApp.Models;

public class MovieImdbResult
{
    public string imdb_id { get; set; }
    public string title { get; set; }
}

public class MovieImdbResultsList
{
    public List<MovieImdbResult> results { get; set; }
}

public class Genre
{
    public int id { get; set; }
    public string genre { get; set; }
}

public class MovieImdbContent
{
    public string imdb_id { get; set; }
    public string title { get; set; }
    public int year { get; set; }
    public string description { get; set; }
    public double rating { get; set; }
    public string image_url { get; set; }
    public string type { get; set; }
    public List<Genre> gen { get; set; }
}

public class MovieImdbContentResult
{
    public MovieImdbContent results { get; set; }
}